# Access Restricted Group Vaults


## About

ARGV (short for Access Restricted Group Vaults), is an Access Based resource sharing application. Using the application, users can store and share data files, only with the people they want to. With a high level of granularity in the amount of access provided to a specific user, people can control who does what with their shared files.

### Technologies used
- Platform & Tools
    - [NodeJS](https://nodejs.org/en)
    - [npm](https://www.npmjs.com/)
    - [pm2](https://pm2.keymetrics.io/) (for managing during cloud deployment)
    - [Dotenv](https://www.dotenv.org/)
- Frontend
    - [React](https://react.dev/)
    - [Material UI (MUI)](https://mui.com/)
    - [Axios](https://axios-http.com/) 
    - [React Hooks](https://react.dev/reference/react/hooks)
    - [React Router](https://reactrouter.com/en/main)
- Backend
    - [Express](https://expressjs.com/)
    - [Mongoose](https://mongoosejs.com/)
    - [JSON Web Tokens](https://jwt.io/)
    - [bcrypt](https://en.wikipedia.org/wiki/Bcrypt) 
    - [AWS SDK](https://docs.aws.amazon.com/sdk-for-javascript/v3/developer-guide/welcome.html) (for JavaScript)
- Database - 
	- [MongoDB](https://www.mongodb.com/)
	- [MongoDB Atlas Database](https://www.mongodb.com/atlas/database)
- Cloud Services - 
    - [AWS S3](https://aws.amazon.com/s3/)
    
- Testing - 
	- [Jest](https://jestjs.io/)
	- [Supertest](https://www.npmjs.com/package/supertest)

### How the application works
[![alt-text](./design_ss/user_access.png "User Access")](https://whimsical.com/user-registration-P1ic5AdyxdmopEkuLtB5tz)

Users are allowed to access two types of spaces, Shared and Personal.
Personal spaces are limited to a single person, with full access to all its data at the topmost Level (Level 4).

Groups are spaces where people can store and collaborate on files in a common space. The creator of the group can define who will have access to resources in these areas.

[![alt-text](./design_ss/user_functions.png "User Access")](https://whimsical.com/user-registration-P1ic5AdyxdmopEkuLtB5tz)

### Levels of Access

- Level 0 - No Access
- Level 1 - View Access
- Level 2 - View, Create, Update Access
- Level 3 - View, Create, Update, Delete Access

### Functionalities
- Register
- Login
- Full access to control files in their personal space
- Create new groups
- Manage Users in groups (groups that they created)
- Join existing groups
- View, Add, Update, Delete files (depending on access provided to them)


## Getting started with ARGV

To make it easy for you to get started with this project, here's a list of recommended next steps.

### Requirements

#### MongoDB
You will need to have a MongoDB connection setup, either in a local or cloud environment. For local environment setup, you can use MongoDB server. The installation steps are present on [MongoDB installation page](https://www.mongodb.com/docs/manual/installation/)

For cloud setups, you can use MongoDB Atlas for deploying a Database. MongoDB Atlas provides various tools and features, you can check all the details on [this page](https://www.mongodb.com/atlas/database).
Atlas also provides various ways for deployment, you can check them all the steps [here](https://www.mongodb.com/docs/atlas/tutorial/deploy-free-tier-cluster/).

Apart from Atlas, you can use a variety of ways of deploying MongoDB servers using the [Cloud Manager Service](https://www.mongodb.com/products/tools/cloud-manager). You can see all the requirements and steps for the same mentioned on [this page](https://www.mongodb.com/docs/cloud-manager/tutorial/provisioning-prep/).

Once done with this, use the connection string in the ```.env.main``` file in server directory.

#### AWS S3 Bucket
This project utilizes AWS Cloud S3 bucket for storing the files for the users. We will need an S3 bucket ready with proper policies and setup for the application.

Simple Storage Service (S3) by AWS is a simple way to store data on a cloud service with high levels of availability, security and flexibility. We use various functions from the [SDK](https://www.npmjs.com/package/aws-sdk) available for NodeJS, allowing us to interact with the bucket effecively.

For creating a new bucket, you can follow the steps mentioned [here](https://docs.aws.amazon.com/AmazonS3/latest/userguide/creating-bucket.html). Note that we don't need a versioned bucket for this project.

Now we will need to setup the Access Control List (ACL) policy for our bucket. This will be required so that our application can surpass the blocking from the bucket and access the files. 

![alt-text](./design_ss/user_functions.png "Bucket Policy")

On the bucket page, go to permissions tab, scroll down and click on 'Edit ACL' option. Now in the editor, enter the following provided policy code and save the changes. Note that you need to change the placeholder for the bucket name here.

```
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Sid": "Deny a presigned URL request if the signature is more than 2 min old",
			"Effect": "Deny",
			"Principal": {
				"AWS": "*"
			},
			"Action": "s3:*",
			"Resource": "arn:aws:s3:::<BUCKET_NAME_HERE>/*",
			"Condition": {
				"NumericGreaterThan": {
					"s3:signatureAge": "120000"
				}
			}
		}
	]
}
```

Now, you need to configure a user for the bucket to access the bucket objects. For this, go to the IAM service tab and then the Users tab (under Access Management). Now click on create user, enter the name (no need to provide access to AWS console), and then on the next page select the option to attack policies directly. Here, you need to search for 'AmazonS3FullAccess' option, and check that option. Click on next and then create.

You will be redirected to the Users page, and it will now show the new user created with the name given. Click on the name, go to Security Credentials Tab, and click on Create Access Key (under the Access Keys area). Here, select 'Local Code', check the confirmation and click on Next > Create Access Key. Download a .csv file for the access keys (these are only available to view once, if you lose them you will need to create a new user). Keep this file safe and secure; if any other user gets access of these keys you might encounter breach on the bucket and potential costs associated with the cloud usage.

Use these keys, bucket name and region of the bucket in the ```.env.main``` file in server directory. You may also need to set these into the development environment you are using, in that case you can see [this](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html) page for steps regarding the same.

### Run this project

You can run this project using the following commands - 

```
git clone https://gitlab.com/NaK915/argv-application.git
cd argv-application
```

Before running, create a ```.env.main``` file in the server and ```.env``` file for frontend and add all environment variables (specified in the ```.env.sample``` files in respective folders; for more information see above)

* For frontend - 
```
cd frontend
npm i
npm run start
```

* For backend -
```
cd server
npm i
node ./index.js
```

### Testing

Test coverage report for the backend of this project is available on [this](https://argv-application-nak915-56e9ad05c8cb1b6998d923313a1003f3c2bf578.gitlab.io/server/coverage/lcov-report/) link. You can see the various files and what parts are covered inside these.

You can run these tests yourself once all the above steps are completed. For this, you need to create a ```.env.test``` file from the sample file in the server directory, and use that to test the backend. Note that the database and S3 bucket credentials used for test should be separate than the main production services.


<!-- ## Test and Deploy

Use the built-in continuous integration in GitLab.

- [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

*** -->