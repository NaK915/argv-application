import React, { useState, useEffect } from 'react';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Backdrop from '@mui/material/Backdrop';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import Typography from '@mui/material/Typography';
import { Button, TextField } from '@mui/material';
import axios from 'axios';

export const ManageAccess = ({ userProfile, setOpen, setSnackbarText }) => {
    // Modal functions
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 500,
        height: 300,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };
    const [modalOpen, setModalOpen] = React.useState(false);
    const handleOpen = (type) => {
        setModalType(type);
        setModalOpen(true);
    }
    const handleClose = () => {
        setModalOpen(false);
    };
    
    // Pagination functions
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
      };
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    // Table Data
    const [rows, setRows] = useState([]);
    const columns = [
        { id: 'name', label: 'Name', minWidth: 170 },
        { id: 'level', label: 'Level', minWidth: 100 },
        { id: 'change', label: 'Change Level', minWidth: 100 },
        { id: 'remove', label: 'Remove from Group', minWidth: 50 }
    ];

    const removeAccess = (id) => {
        // console.log(id);
        // console.log(currentGroup._id);
        axios.delete(`${process.env.REACT_APP_API_URL}group/level?_id=${currentGroup._id}&profile_id=${id}`).then((response) => {
            setSnackbarText('Deleted Successfully');
            setOpen(true);
            setTimeout(() => {
                setRows(response.data.access.map((item) => ({
                    name: item.name,
                    // _id: item._id,
                    level: item.level,
                    change: currentGroup.level === 4 && item._id !== userProfile._id ? <Button variant='contained' onClick={() => {editAccess(item)}} >Edit Access</Button> : <Button variant='contained' disabled>Edit Access</Button>,
                    remove: currentGroup.level === 4 && item._id !== userProfile._id ? <Button variant='contained' onClick={() => {removeAccess(item._id)}} >Remove Access</Button> : <Button variant='contained' disabled>Remove Access</Button>
                })));
            }, 2000);
        }).catch((err) => {
            if(err.response.status===498){
                delete axios.defaults.headers.common['Authorization'];
                sessionStorage.clear();
                navigator('/access');
            } else {
                setSnackbarText(err.message);
                setOpen(true);
            }
        })
    }

    const [level, setLevel] = useState('');
    const addAccess = (e) => {
        e.preventDefault();
        console.log('123');
        const email = document.getElementById('add-user-email').value;
        axios.get(`${process.env.REACT_APP_API_URL}profile/email?email=${email}`).then((response) => {
            const data = {
                profile: {
                    name: response.data.name,
                    _id: response.data._id,
                    level: Number(level)
            }};
            console.log(data);
            axios.post(`${process.env.REACT_APP_API_URL}group/level?_id=${currentGroup._id}`, data).then((out) => {
                setSnackbarText('Added Successfully!');
                setOpen(true);
                setTimeout(() => {
                    setLevel('');
                    setRows(out.data.access.map((item) => ({
                        name: item.name,
                        // _id: item._id,
                        level: item.level,
                        change: currentGroup.level === 4 && item._id !== userProfile._id ? <Button variant='contained' onClick={() => {editAccess(item)}} >Edit Access</Button> : <Button variant='contained' disabled>Edit Access</Button>,
                        remove: currentGroup.level === 4 && item._id !== userProfile._id ? <Button variant='contained' onClick={() => {removeAccess(item._id)}} >Remove Access</Button> : <Button variant='contained' disabled>Remove Access</Button>
                    })));
                    setModalOpen(false);
                }, 3500);
            }).catch((err) => {
                setSnackbarText(err.message);
                setOpen(true);
            })
        }).catch((err) => {
            setSnackbarText(err.response.data.message);
            setOpen(true);
        });
    }

    const [modalType, setModalType] = useState('');
    const [editUser, setEditUser] = useState({});
    const [sendPut, setSendPut] = useState(false);
    const editAccess = (user) => {
        console.log(user);
        setEditUser(user);
        handleOpen('edit');
        setTimeout(() => {
            document.getElementById('add-user-email').value = user._id;
            document.getElementById('add-user-email').disabled=true;
            document.getElementById('add-user-email').focus(); 
        }, 100);
    }

    const editA = (e) => {
        e.preventDefault();
        if(level===''){
            setSnackbarText('No level selected');
            setOpen(true);
        } else {
            const email = document.getElementById('add-user-email').value;
            setEditUser({
                ...editUser,
                level: Number(level)
            });
            setSendPut(true);
        }
    }

    useEffect(() => {
        console.log(editUser);
    }, [editUser]);

    useEffect(() => {
        if(sendPut){
            axios.put(`${process.env.REACT_APP_API_URL}group/level?_id=${currentGroup._id}`, {profile: editUser}).then((out) => {
                setSnackbarText('Changed Access Successfully');
                setOpen(true);
                setTimeout(() => {
                    setLevel('');
                    setRows(out.data.access.map((item) => ({
                        name: item.name,
                        // _id: item._id,
                        level: item.level,
                        change: currentGroup.level === 4 && item._id !== userProfile._id ? <Button variant='contained' onClick={() => {editAccess(item)}} >Edit Access</Button> : <Button variant='contained' disabled>Edit Access</Button>,
                        remove: currentGroup.level === 4 && item._id !== userProfile._id ? <Button variant='contained' onClick={() => {removeAccess(item._id)}} >Remove Access</Button> : <Button variant='contained' disabled>Remove Access</Button>
                    })));
                    setModalOpen(false);
                }, 3500);
            }).catch((err) => {
                setSnackbarText(err.message);
                setOpen(true);
            });
        }
    }, [sendPut])

    // Group Selection
    const [groups, setGroups] = useState([]);
    const [currentGroup, setCurrentGroup] = useState(null);
    useEffect(() => {
        if(userProfile !== null){
            // console.log(userProfile.access);
            setGroups(userProfile.access);
        }
    }, [userProfile]);

    const getGroup = () => {
        axios.get(`${process.env.REACT_APP_API_URL}group?_id=${currentGroup._id}`).then((response) => {
            console.log(currentGroup.level);
            // console.log();
            setRows(response.data.access.map((item) => ({
                name: item.name,
                // _id: item._id,
                level: item.level,
                change: currentGroup.level === 4 && item._id !== userProfile._id ? <Button variant='contained' onClick={() => {editAccess(item)}} >Edit Access</Button> : <Button variant='contained' disabled>Edit Access</Button>,
                remove: currentGroup.level === 4 && item._id !== userProfile._id ? <Button variant='contained' onClick={() => {removeAccess(item._id)}} >Remove Access</Button> : <Button variant='contained' disabled>Remove Access</Button>
            })));
            console.log(response.data.access);
        }).catch((err) => {
            console.log(err.message);
            setSnackbarText(err.message);
            setOpen(true);
        });
    }

    useEffect(() => {
        if(currentGroup!==null){
            document.getElementById('access').style.display='block'
            getGroup();
        }
    }, [currentGroup]);

  return (
    <div>
        <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={modalOpen}
        onClose={handleClose}
        closeAfterTransition
        slots={{ backdrop: Backdrop }}
        slotProps={{
          backdrop: {
            timeout: 500,
          },
        }}
      >
        <Fade in={modalOpen}>
          <Box sx={style}>
            {modalType === 'add' ? <>
            <Typography sx={{marginBottom: '5%', color: '#fff'}} id="transition-modal-title" variant="h6" component="h2">
              ADD NEW USER
            </Typography>
            <form onSubmit={addAccess} id='add-user' style={{display: 'flex', gap: '3%', flexDirection: 'column', height: '80%'}}>
                <InputLabel>Enter Email</InputLabel>
                <TextField fullWidth id="add-user-email" variant="outlined" />
                <InputLabel id="demo-simple-select-label">Level</InputLabel>
                <Select
                    sx={{width: '40%'}}
                    labelId="demo-simple-select-label"
                    id="demo-simple-select-add-user"
                    value={level}
                    label="Level"
                    onChange={(newValue) => setLevel(newValue.target.value)}
                >
                    <MenuItem value={1}>1 - View</MenuItem>
                    <MenuItem value={2}>2 - View/Create/Update</MenuItem>
                    <MenuItem value={3}>3 - View/Create/Update/Delete</MenuItem>
                </Select>
                <Button sx={{alignSelf: 'center', margin: 'auto'}} type="submit" variant='contained' >Add user</Button>
            </form></> : 
            <>
                <Typography sx={{marginBottom: '5%', color: '#fff'}} id="transition-modal-title" variant="h6" component="h2">
                    EDIT USER
                </Typography>
                <form onSubmit={editA} id='add-user' style={{display: 'flex', gap: '3%', flexDirection: 'column', height: '80%'}}>
                    <InputLabel>ID</InputLabel>
                    <TextField fullWidth id="add-user-email" variant="outlined" />
                    <InputLabel id="demo-simple-select-label">Level</InputLabel>
                    <Select
                        sx={{width: '40%'}}
                        labelId="demo-simple-select-label"
                        id="demo-simple-select-add-user"
                        value={level}
                        label="Level"
                        onChange={(newValue) => setLevel(newValue.target.value)}
                    >
                        <MenuItem value={1}>1 - View</MenuItem>
                        <MenuItem value={2}>2 - View/Create/Update</MenuItem>
                        <MenuItem value={3}>3 - View/Create/Update/Delete</MenuItem>
                    </Select>
                    <Button sx={{alignSelf: 'center', margin: 'auto'}} type="submit" variant='contained' >Edit User Access</Button>
                </form>
            </>
            }
          </Box>
        </Fade>
      </Modal>
        <FormControl fullWidth sx={{marginTop: '5%', marginBottom: '5%', display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
            <InputLabel id="demo-simple-select-label">Group</InputLabel>
            <Select
                sx={{width: '40%'}}
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={currentGroup}
                label="Group"
                onChange={(newValue) => setCurrentGroup(newValue.target.value)}
            >
                {groups.map((group) => {
                    return <MenuItem value={group}>{group.name}</MenuItem>
                })}
            </Select>
            <Button onClick={() => handleOpen('add')} variant='outlined' id="access" sx={{display: 'none', width: '25%'}}>Add new user</Button>
        </FormControl>
        <Paper sx={{ width: '100%', overflow: 'hidden' }}>
            <TableContainer sx={{ maxHeight: 440 }}>
                <Table stickyHeader aria-label="sticky table">
                <TableHead>
                    <TableRow>
                    {columns.map((column) => (
                        <TableCell
                        key={column.id}
                        align={column.align}
                        style={{ minWidth: column.minWidth }}
                        >
                        {column.label}
                        </TableCell>
                    ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row) => {
                        return (
                        <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                            {columns.map((column) => {
                            const value = row[column.id];
                            return (
                                <TableCell key={column.id} align={column.align}>
                                {column.format && typeof value === 'number'
                                    ? column.format(value)
                                    : value}
                                </TableCell>
                            );
                            })}
                        </TableRow>
                        );
                    })}
                </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[10, 25, 100]}
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </Paper>
    </div>
  )
}
