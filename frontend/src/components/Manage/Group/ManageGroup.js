import React, { useEffect, useState } from 'react';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import axios from 'axios';
import { Button } from '@mui/material';

export const ManageGroup = ({ userProfile, setUserProfile, setSnackbarText, setOpen }) => {

    // Pagination functions
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    // Table Data
    const [rows, setRows] = useState([]);
    const columns = [
        { id: 'name', label: 'Name', minWidth: 170 },
        { id: 'created', label: 'Created', minWidth: 100 },
        { id: 'leave', label: 'Leave Group', minWidth: 50 },
        { id: 'delete', label: 'Delete Group', minWidth: 50 }
    ];

    // Get User Data
    const getUserData = async () => {
        const user = await sessionStorage.getItem('_id');
        axios.get(`${process.env.REACT_APP_API_URL}profile?_id=${user}`).then((response) => {
            setUserProfile(response.data);
        }).catch((err) => {
            if(err.response.status===498){
                delete axios.defaults.headers.common['Authorization'];
                sessionStorage.clear();
                navigator('/access');
            }
            console.log(err);
        });
      }
  
      useEffect(() => {
        getUserData();
      }, []);

      // Leave group
      const leaveGroup = (id) => {
        console.log(id);
      }

      // Delete group
      const deleteGroup = (id) => {
        axios.delete(`${process.env.REACT_APP_API_URL}group?_id=${id}`).then((response) => {
            setSnackbarText(response.data.message);
            setOpen(true);
            setTimeout(() => {
                getUserData();
            }, 3500);
        })
      }

      // Set Rows for table
      useEffect(() => {
        console.log(userProfile)
        setRows(userProfile.access.map((item) => ({
            name: item.name,
            created: item.created,
            leave: item.level === 4 ? <Button variant='outlined' disabled>Leave Group</Button> : <Button variant='outlined' onClick={() => leaveGroup(userProfile._id)}>Leave Group</Button>,
            delete: item.level === 4 ? <Button variant='contained' onClick={() => {deleteGroup(item._id)}} >Delete Group</Button> : <Button variant='contained' disabled>Delete Group</Button>
        })));
      }, [userProfile]);

  return (
    <div style={{width: '100%'}}>
        <Paper sx={{ width: '100%', overflow: 'hidden' }}>
            <TableContainer sx={{ maxHeight: 440 }}>
                <Table stickyHeader aria-label="sticky table">
                <TableHead>
                    <TableRow>
                    {columns.map((column) => (
                        <TableCell
                        key={column.id}
                        align={column.align}
                        style={{ minWidth: column.minWidth }}
                        >
                        {column.label}
                        </TableCell>
                    ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row) => {
                        return (
                        <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                            {columns.map((column) => {
                            const value = row[column.id];
                            return (
                                <TableCell key={column.id} align={column.align}>
                                {column.format && typeof value === 'number'
                                    ? column.format(value)
                                    : value}
                                </TableCell>
                            );
                            })}
                        </TableRow>
                        );
                    })}
                </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[10, 25, 100]}
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </Paper>
    </div>
  )
}
