import React, { useState } from 'react';
import TextField from '@mui/material/TextField';
import InputBase from '@mui/material/InputBase';
import Snackbar from '@mui/material/Snackbar';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import { styled } from '@mui/material/styles';
import Button from '@mui/material/Button';
import './group.css';
import { ManageGroup } from '../Manage/Group/ManageGroup';
import { ManageAccess } from '../Manage/Access/ManageAccess';
import axios from 'axios';

const BootstrapInput = styled(InputBase)(({ theme }) => ({
  'label + &': {
    marginTop: theme.spacing(3),
  },
  '& .MuiInputBase-input': {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '10px 26px 10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
    },
  },
}));

export const Group = ({ current, setCurrent, userProfile, setUserProfile }) => {
  // Snackbar component
  const [open, setOpen] = useState(false);
  const [snackbarText, setSnackbarText] = useState('');
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };
  const action = (
    <React.Fragment>
      {/* <Button color="secondary" size="small" onClick={handleClose}>
        UNDO
      </Button> */}
      <IconButton
        size="small"
        aria-label="close"
        color="inherit"
        onClick={handleClose}
      >
        <CloseIcon fontSize="small" />
      </IconButton>
    </React.Fragment>
  );


  // Creating a new Group
  const createNewGroup = (e) => {
    e.preventDefault();
    const data = {
      name: document.getElementById('group-name').value,
      // access: document.getElementById('access').value !== '' ? document.getElementById('access').value.split(',') : [],
    }
    console.log(data);
    axios.post(`${process.env.REACT_APP_API_URL}group`, data).then((response) => {
      console.log(response);
      setSnackbarText('Created new group successfully');
      setOpen(true);
      setTimeout(() => {
        setCurrent('manage');
      }, 3500);
    }).catch((err) => {
      if(err.response.status===498){
        delete axios.defaults.headers.common['Authorization'];
        sessionStorage.clear();
        navigator('/access');
      } else {
        // console.log(err);
        err.response.status >=500 ? setSnackbarText('Server Error') : setSnackbarText(err.message);
        setOpen(true);
      }
    });
  }

  return (
    <div id='main'>
      {current === "manage" ? <>
        <ManageGroup setOpen={setOpen} setSnackbarText={setSnackbarText} userProfile={userProfile} setUserProfile={setUserProfile} />
      </> : current === 'create' ? <>
        <form id='create-group-form' onSubmit={createNewGroup}>
          <TextField id="group-name" label="Group Name" variant="outlined" required />
          {/* <div>
            <InputLabel htmlFor="outline-basic">Add Access</InputLabel>
            <TextField id="access" label="comma(,) separated Mail IDs" sx={{mt:1}} variant="outlined" fullWidth />
          </div> */}
          <Button type='submit' id="create-button" variant="contained">Create</Button>
        </form>
      </> : <>
        <ManageAccess userProfile={userProfile} setOpen={setOpen} setSnackbarText={setSnackbarText} />
      </>}
      <Snackbar
        open={open}
        autoHideDuration={3000}
        onClose={handleClose}
        message={snackbarText}
        action={action}
      />
    </div>
  )
}
