import React, { useEffect, useState } from 'react'
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Snackbar from '@mui/material/Snackbar';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import { Group } from '../Group/Group';
import { FileUploader } from "react-drag-drop-files";
import { Button } from '@mui/material';
import axios from 'axios';
import './main.css';

export const Main = ({ files, current, setCurrent, userProfile, setUserProfile, currentGroup }) => {
    // Table Data
    const [rows, setRows] = useState([]);
    const columns = [
        { id: 'name', label: 'Name', minWidth: 170 },
        { id: 'modified', label: 'Last Modified', minWidth: 100 },
        { id: 'modified_by', label: 'Last Modified By', minWidth: 100 },
        { id: 'author', label: 'Author', minWidth: 100 },
        { id: 'created', label: 'Created', minWidth: 100 },
        { id: 'download', label: 'Download', minWidth: 50 },
        { id: 'delete', label: 'Delete', minWidth: 50 }
    ];

    // Snackbar component
    const [open, setOpen] = useState(false);
    const [snackbarText, setSnackbarText] = useState('');
    const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
        return;
    }
    setOpen(false);
    };
    const action = (
    <React.Fragment>
        {/* <Button color="secondary" size="small" onClick={handleClose}>
        UNDO
        </Button> */}
        <IconButton
        size="small"
        aria-label="close"
        color="inherit"
        onClick={handleClose}
        >
        <CloseIcon fontSize="small" />
        </IconButton>
    </React.Fragment>
    );
    
    const getFile = (type, id, name) => {
        if(type === 'personal'){
            // console.log(type);
            // console.log(id);
            // console.log(name);
            axios.get(`${process.env.REACT_APP_API_URL}files/personal?_id=${id}&name=${name}`).then((response) => {
                window.open(response.data.url, '_blank');
            }).catch((err) => {
                console.log(err);
                setSnackbarText(err.response.data.message);
                setOpen(true);
            });
        } else {
            // console.log(type);
            // console.log(id);
            // console.log(name);
            axios.get(`${process.env.REACT_APP_API_URL}files?_id=${id}&name=${name}`).then((response) => {
                window.open(response.data.url, '_blank');
            }).catch((err) => {
                console.log(err);
                setSnackbarText(err.response.data.message);
                setOpen(true);
            });
        }
    }

    const deleteFile = (name) => {
        console.log(name);
        if(currentGroup.name==='Personal'){
            axios.delete(`${process.env.REACT_APP_API_URL}files/personal?_id=${userProfile._id}&file_name=${name}`).then((response) => {
                console.log(response);
                setRows(response.data.personal.map((item) => ({
                    name: item.name,
                    modified: new Date(item.modified).toString(),
                    modified_by: item.modified_by.email,
                    author: item.author.email,
                    created: new Date(item.created).toString(),
                    download: <Button variant="outlined" onClick={() => getFile('personal', userProfile._id, item.name)} >Download File</Button>,
                    delete: <Button variant='contained' onClick={() => deleteFile(item.name)}>Delete File</Button>
                })));
            }).catch((err) => {
                if(err.response.status===498){
                    delete axios.defaults.headers.common['Authorization'];
                    sessionStorage.clear();
                    navigator('/access');
                }
                console.log(err);
            });
        } else {
            axios.delete(`${process.env.REACT_APP_API_URL}files?_id=${currentGroup._id}&file_name=${name}`).then((response) => {
                console.log(response);
                setRows(response.data.files.map((item) => ({
                    name: item.name,
                    modified: new Date(item.modified).toString(),
                    modified_by: item.modified_by.email,
                    author: item.author.email,
                    created: new Date(item.created).toString(),
                    download: <Button variant="outlined" onClick={() => getFile('group', currentGroup._id, item.name)} >Download File</Button>,
                    delete: currentGroup.level >= 3 ? <Button variant='contained' onClick={() => deleteFile(item.name)}>Delete File</Button> : <Button variant='contained' disabled >Delete File</Button>
                })));
            }).catch((err) => {
                if(err.response.status===498){
                    delete axios.defaults.headers.common['Authorization'];
                    sessionStorage.clear();
                    navigator('/access');
                }
                console.log(err);
            });
        }
    }

    const getFiles = () => {
        if(currentGroup.name==='Personal'){
            // setRows(userProfile.personal)
            setRows(userProfile.personal.map((item) => ({
                name: item.name,
                modified: new Date(item.modified).toString(),
                modified_by: item.modified_by.email,
                author: item.author.email,
                created: new Date(item.created).toString(),
                download: <Button variant="outlined" onClick={() => getFile('personal',userProfile._id, item.name)} >Download File</Button>,
                delete: <Button variant='contained' onClick={() => deleteFile(item.name)}>Delete File</Button>
            })));
        } else {
            axios.get(`${process.env.REACT_APP_API_URL}group?_id=${currentGroup._id}`).then((response) => {
                setRows(response.data.files.map((item) => ({
                    name: item.name,
                    modified: new Date(item.modified).toString(),
                    modified_by: item.modified_by.email,
                    author: item.author.email,
                    created: new Date(item.created).toString(),
                    download: <Button variant="outlined" onClick={() => getFile('group', currentGroup._id, item.name)} >Download File</Button>,
                    delete: currentGroup.level >= 3 ? <Button variant='contained' onClick={() => deleteFile(item.name)}>Delete File</Button> : <Button variant='contained' disabled >Delete File</Button>
                })));
            }).catch((err) => {
                if(err.response.status===498){
                    delete axios.defaults.headers.common['Authorization'];
                    sessionStorage.clear();
                    navigator('/access');
                }
                console.log(err.message);
            })
        }
    }

    useEffect(() => {
        if(currentGroup!==''){
            getFiles();
        }
    }, [currentGroup]);

    // Pagination function
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    // Upload files
    const handleChange = (file) => {
        // console.log(currentGroup.level);
        console.log(file);
        if(currentGroup.level > 1 || currentGroup.name === 'Personal'){
            const formData = new FormData();
            formData.append('file', file);
            if(currentGroup._id === 'personal'){
                axios.post(`${process.env.REACT_APP_API_URL}upload/personal?_id=${userProfile._id}`, formData).then((response) => {
                    console.log(response);
                    setRows(response.data.personal.map((item) => ({
                        name: item.name,
                        modified: new Date(item.modified).toString(),
                        modified_by: item.modified_by.email,
                        author: item.author.email,
                        created: new Date(item.created).toString(),
                        download: <Button variant="outlined" onClick={() => getFile('personal', userProfile._id, item.name)} >Download File</Button>,
                        delete: <Button variant='contained' onClick={() => deleteFile(item.name)}>Delete File</Button>
                    })));
                }).catch((err) => {
                    if(err.response.status===498){
                        delete axios.defaults.headers.common['Authorization'];
                        sessionStorage.clear();
                        navigator('/access');
                    }
                    console.log(err.message);
                });
            } else {
                axios.post(`${process.env.REACT_APP_API_URL}upload?_id=${currentGroup._id}`, formData).then((response) => {
                    console.log(response);
                    setRows(response.data.files.map((item) => ({
                        name: item.name,
                        modified: new Date(item.modified).toString(),
                        modified_by: item.modified_by.email,
                        author: item.author.email,
                        created: new Date(item.created).toString(),
                        download: <Button variant="outlined" onClick={() => getFile('group', currentGroup._id, item.name)} >Download File</Button>,
                        delete: currentGroup.level >= 3 ? <Button variant='contained' onClick={() => deleteFile(item.name)}>Delete File</Button> : <Button variant='contained' disabled >Delete File</Button>
                    })));
                }).catch((err) => {
                    if(err.response.status===498){
                        delete axios.defaults.headers.common['Authorization'];
                        sessionStorage.clear();
                        navigator('/access');
                    }
                    console.log(err.message);
                });
            }
        } else {
            setSnackbarText('Access Denied');
            setOpen(true);
        }
    };

    
  return (
    <div>
        {files ? <div style={{display: 'flex', flexDirection: 'column', gap: '2rem', alignItems: 'center'}}>
            <Paper sx={{ width: '100%', overflow: 'hidden' }}>
                <TableContainer sx={{ maxHeight: 440 }}>
                    <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                        {columns.map((column) => (
                            <TableCell
                            key={column.id}
                            align={column.align}
                            style={{ minWidth: column.minWidth }}
                            >
                            {column.label}
                            </TableCell>
                        ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows
                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        .map((row) => {
                            return (
                            <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                                {columns.map((column) => {
                                const value = row[column.id];
                                return (
                                    <TableCell key={column.id} align={column.align}>
                                    {column.format && typeof value === 'number'
                                        ? column.format(value)
                                        : value}
                                    </TableCell>
                                );
                                })}
                            </TableRow>
                            );
                        })}
                    </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                />
            </Paper>
            {currentGroup && (currentGroup.level > 1 || currentGroup.name === 'Personal') ? 
                <FileUploader classes='file-drop-zone' onSizeError={() => {console.log('Max File Size is 25MB')}} multiple={false} maxSize={25} handleChange={handleChange} name="file" />
                : 
                <></>
            }
        </div> : <>
            <Group current={current} setCurrent={setCurrent} userProfile={userProfile} setUserProfile={setUserProfile} />
        </>}
        <Snackbar
        open={open}
        autoHideDuration={3000}
        onClose={handleClose}
        message={snackbarText}
        action={action}
      />
    </div>
  )
}
