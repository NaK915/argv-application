import React, { useEffect, useState } from 'react'
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import Button from '@mui/material/Button';
import './secondbar.css';

export const Secondbar = ({ files, setCurrent, userProfile, setCurrentGroup, currentGroup }) => {

    const [groups, setGroups] = useState([]);

    useEffect(() => {
        if(userProfile !== null){
            setGroups([{name: 'Personal', _id:'personal'}, ...userProfile.access ])
        }
    }, [userProfile]);

    // useEffect(() => {
    //     console.log(groups);
    // }, [groups]);

  return (
    <div id='bar'>
        {files ? 
        <>
            <div id='menu'>
                <FormControl sx={{width: 250}}>
                    <InputLabel id="demo-simple-select-label">Group</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={currentGroup}
                        label="Group"
                        onChange={(newValue) => setCurrentGroup(newValue.target.value)}
                    >
                        {groups.map((group) => {
                            return <MenuItem value={group}>{group.name}</MenuItem>
                        })}
                        {/* <MenuItem value={10}>Ten</MenuItem> */}
                        {/* <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem> */}
                    </Select>
                </FormControl>
            </div>
        </> : <>
            <div id='menu'>
                <Button variant='contained' onClick={() => setCurrent('manage')}>Manage</Button>
                <Button variant='contained' onClick={() => setCurrent('create')}>Create Group</Button>
                <Button variant='contained' onClick={() => setCurrent('access')}>Manage Access</Button>
            </div>
        </>}
    </div>
  )
}
