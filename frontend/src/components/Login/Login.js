import React, { useState } from 'react';
import './login.css';
import TextField from '@mui/material/TextField';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import InputAdornment from '@mui/material/InputAdornment';
import IconButton from '@mui/material/IconButton';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import { FormControl } from '@mui/material';
import Button from '@mui/material/Button';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { ThemeProvider } from '@emotion/react';

export const Login = ({ setAccessType, theme }) => {
  // Show or Hide password functions
  const [showPassword, setShowPassword] = useState(false);
  const handleClickShowPassword = () => setShowPassword((showPassword) => !showPassword);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  
  // Open Register Page
  const openRegister = () => {
    setAccessType('register');
  }

  // Login function
  const navigate = useNavigate();
  const loginUser = (e) => {
    e.preventDefault();
    const data = {
      email: document.getElementById('email').value,
      password: document.getElementById('password').value,
    }
    console.log(data);
    axios.post(`${process.env.REACT_APP_API_URL}login`, data).then((response) => {
      console.log(response.data.token);
      sessionStorage.setItem('token', response.data.token);
      sessionStorage.setItem('_id', response.data._id);
      navigate('/dashboard');
    }).catch((err) => {
      console.log(err.response.data.message);
      document.getElementById('error').style.display = 'block';
      document.getElementById('error').style.color = '#ff0000';
      document.getElementById('error').innerHTML = err.response.status >= 500 ? 'Server Error' : err.response.data.message;
      setTimeout(() => {
        document.getElementById('error').style.display = 'none';
      }, 3000);
    });
  }

  return (
    <div id='login'>
      <text id='main'>Welcome back</text>
      <form id='login-form' onSubmit={loginUser}>
        <TextField id="email" label="Email" variant="outlined" required />
          <FormControl sx={{ m: 0 }} variant="outlined">
          <InputLabel htmlFor="password">Password</InputLabel>
            <OutlinedInput
              required
              id="password"
              type={showPassword ? 'text' : 'password'}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                    edge="end"
                  >
                    {showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
              label="Password"
            />
            </FormControl>
            <Button type='submit' variant="contained">Log me In!</Button>
      </form>
      <text id='error' style={{display: 'none'}}></text>
      <text id='sub'>New around here? <span id='open-register' onClick={openRegister}>Register Here</span></text>
    </div>
  )
}
