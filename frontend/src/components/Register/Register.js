import React, { useState } from 'react';
import './register.css';
import Backdrop from '@mui/material/Backdrop';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import InputAdornment from '@mui/material/InputAdornment';
import IconButton from '@mui/material/IconButton';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import { FormControl } from '@mui/material';
import Button from '@mui/material/Button';
import axios from 'axios';

export const Register = ({ setAccessType }) => {

  // Modal functions
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 900,
    height: 200,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => {
    setOpen(false);
    openLogin();
  };

  // Show or Hide Password text
  const [showPassword, setShowPassword] = useState(false);
  const handleClickShowPassword = () => setShowPassword((showPassword) => !showPassword);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  // Open Login Page
  const openLogin = () => {
    setAccessType('login');
  }

  // Register function
  const [user, setUser] = useState({});
  const registerUser = (e) => {
    e.preventDefault();
    const data = {
      name: document.getElementById('name').value,
      email: document.getElementById('email').value,
      password: document.getElementById('password').value,
      access: [],
      personal: [],
    }
    axios.post(`${process.env.REACT_APP_API_URL}register`, data).then((response) => {
      console.log(response.data.user);
      setUser(response.data.user);
      handleOpen();
    }).catch((err) => {
      console.log(err.response.data.message);
      document.getElementById('error').style.display = 'block';
      document.getElementById('error').style.color = '#ff0000';
      document.getElementById('error').innerHTML = err.response.status === 500 ? 'Server Error' : err.response.data.message;
      setTimeout(() => {
        document.getElementById('error').style.display = 'none';
      }, 3000);
    });
  }

  return (
    <div id='register'>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        slots={{ backdrop: Backdrop }}
        slotProps={{
          backdrop: {
            timeout: 500,
          },
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <Typography color='#fff' id="transition-modal-title" variant="h6" component="h2">
              Welcome to your vault {user.name}!
            </Typography>
            {/* <Typography id="transition-modal-description" sx={{ mt: 2 }}>
              Before Logging in, copy this secure key and store it in a safe file. In case you forget your password, we will need this to confirm your identity. Do not share this key with anyone.<br />
              <span id='key'>{user.key}</span>
            </Typography> */}
          </Box>
        </Fade>
      </Modal>
      <text id='main'>Hola!</text>
      <form id='register-form' onSubmit={registerUser}>
        <TextField id="name" label="Name" variant="outlined" required />
        <TextField id="email" type='email' label="Email" variant="outlined" required />
        <FormControl sx={{ m: 0 }} variant="outlined">
          <InputLabel htmlFor="password">Password</InputLabel>
            <OutlinedInput
              required
              id="password"
              type={showPassword ? 'text' : 'password'}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                    edge="end"
                  >
                    {showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
              label="Password"
            />
          </FormControl>
          <Button type="submit" variant="contained">Register Me!</Button>
      </form>
      <text id='error' style={{display: 'none'}}></text>
      <text id='sub'>Already a User? <span id='open-login' onClick={openLogin}>Login Here</span></text>
    </div>
  )
}
