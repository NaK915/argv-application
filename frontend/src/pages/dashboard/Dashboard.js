import React, { useEffect, useState } from 'react'
import Navbar from '../../components/Navbar/Navbar'
import { Secondbar } from '../../components/SecondBar/Secondbar'
import { Main } from '../../components/Main/Main'
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import './dashboard.css';

export const Dashboard = ({}) => {
    const [files, setFiles] = useState(true);
    const [current, setCurrent] = useState('manage');
    const [currentGroup, setCurrentGroup] = useState("");
    
    const [userProfile, setUserProfile] = useState(null);
    const navigator = useNavigate();
    const getUserData = async () => {
      const user = await sessionStorage.getItem('_id');
      axios.get(`${process.env.REACT_APP_API_URL}profile?_id=${user}`).then((response) => {
        setUserProfile(response.data);
      }).catch((err) => {
        if(err.response.status===498){
          delete axios.defaults.headers.common['Authorization'];
          sessionStorage.clear();
          navigator('/access');
        }
        console.log(err);
      });
    }
    
    useEffect(() => {
      axios.defaults.headers.common['Authorization'] = sessionStorage.getItem('token');
      getUserData();
    }, []);

  return (
    <div id='dashboard'>
        <Navbar setFiles={setFiles} files={files} userProfile={userProfile} setUserProfile={setUserProfile} />
        <Secondbar currentGroup={currentGroup} setCurrentGroup={setCurrentGroup} files={files} setCurrent={setCurrent} userProfile={userProfile} />
        <Main currentGroup={currentGroup} files={files} setCurrent={setCurrent} current={current} userProfile={userProfile} setUserProfile={setUserProfile} />
    </div>
  )
}
