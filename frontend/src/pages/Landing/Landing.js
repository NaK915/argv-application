import React, { useEffect } from 'react'
import logo from '../../Assets/argv-color-transparent.png';
import secure from '../../Assets/secure.svg';
import role from '../../Assets/role.svg';
import fast from '../../Assets/fast.svg';
import user_access from '../../Assets/user_access.png';
import access from '../../Assets/access.png';
import levels from '../../Assets/levels.png';
import './Landing.css';

export const Landing = () => {
    const animation = () => {
        var heroClassAnimate = 'animate';
        var Hero = {
            init: function () {
            this.setDOMSelectors();
            this.render();
        },
        setDOMSelectors: function () {
            this.header = document.querySelector('.js-header');
            this.heroContainer = document.querySelector('.js-hero');
        },
        render: function () {
            var self = this;
            setTimeout(function () {
                self.heroContainer?.classList.add(heroClassAnimate);
            }, 1);
            }
        };
        Hero.init();
    }

    useEffect(() => {
        if(document.querySelector('.js-hero')){
            animation();
        }
    }, [document.querySelector('.js-hero')]);

  return (
    <div id='body'>
        <nav>
            <a href='/'>
                <img src={logo} alt='' />
            </a>
            <button onClick={() => {window.location.href='/access'}}>Access your vault</button>
        </nav>
        <div id='land'>
            <div class="hero js-hero">
                <div class="hero__curtain">
                    <div class="hero__curtain-1"></div>
                    <div class="hero__curtain-2">
                        <div class="hero__diagonal"></div>
                    </div>
                </div>

                <section class="hero__content">
                    <h1 class="hero__content-title h1">Access Restricted Group Vaults</h1>
                    <p class="hero__content-text text text--hero">Share files with ease and security</p>
                </section>
            </div>
        </div>
        <section id='cards'>
            <div id='cards-heading'>Features</div>
            <div id='cards-list'>
                <div className='card'>
                    <div className='logo'>
                        <img src={secure} alt='' />
                    </div>
                    <div className='text'>
                        <div className='title'>Secure</div>
                        <div className='body'>Uses multiple layers of security, ensuring everything you upload remains safe.</div>
                    </div>
                </div>
                <div className='card'>
                    <div className='logo'>
                        <img src={role} alt='' />
                    </div>
                    <div className='text'>
                        <div className='title'>Role based Restrictions</div>
                        <div className='body'>Restriction on who can perform operations on files based on level of users.</div>
                    </div>
                </div>
                <div className='card'>
                    <div className='logo'>
                        <img src={fast} alt='' />
                    </div>
                    <div className='text'>
                        <div className='title'>Fast and User friendly</div>
                        <div className='body'>Fast and user friendly platform to ensure users can share files efficiently.</div>
                    </div>
                </div>
            </div>
        </section>
        <section id='working'>
            <div id='heading'>
                How it all works
            </div>
            <div id='cards-list'>
                <div className='card-type2'>
                    <div className='logo'>
                        <img src={user_access} alt='' />
                    </div>
                    <div className='text'>
                        <div className='title'>Personal and Group Vaults</div>
                        <div className='body'>Users have their own personal vaults, and group sharing vaults with access limiting.</div>
                    </div>
                </div>
                <div className='card-type2'>
                    <div className='logo'>
                        <img src={levels} alt='' />
                    </div>
                    <div className='text'>
                        <div className='title'>Varied Access Levels</div>
                        <div className='body'>Varied Access Levels to provide granular control over who manages group files.</div>
                    </div>
                </div>
                <div className='card-type2'>
                    <div className='logo'>
                        <img src={access} alt='' />
                    </div>
                    <div className='text'>
                        <div className='title'>Control Access for Group</div>
                        <div className='body'>Only the creator gets final access for group members, ensuring secure control.</div>
                    </div>
                </div>
            </div>
        </section>
        <footer>
            © 2024 ARGV. All Rights Reserved.
            ARGV and ARGV logo are trademarks of ARGV in Bharat and/or other countries. 
        </footer>
    </div>
  )
}
