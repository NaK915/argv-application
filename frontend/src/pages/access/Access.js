import React, { useEffect, useState } from 'react';
import { Register } from '../../components/Register/Register';
import { Login } from '../../components/Login/Login';
import image from '../../Assets/image.png';
import './access.css'
import { useNavigate } from 'react-router-dom';

export const Access = ({ setUser, theme }) => {
    const [accessType, setAccessType] = useState('login');
    const navigate = useNavigate();
    useEffect(() => {
        if(sessionStorage.getItem('token')){
            navigate('/dashboard');
        }
    }, [])
  return (
    <div id='full'>
        <div id='left'>
            {accessType !== 'login' ? 
                <Register theme={theme} setAccessType = {setAccessType} />
                :
                <Login theme={theme} setUser={setUser} setAccessType = {setAccessType} />
            }
        </div>
        <div id='right'>
            <text id='main'>Share Data with Ease and Security</text>
            <img src = {image} />
            <text id='sub'>Our solution provides you a fully managed solution to sharing files securely, so that you and your organization don't need to worry about information leaks.</text>
        </div>
    </div>
  )
}
