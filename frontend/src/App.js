import { Access } from "./pages/access/Access";
import './App.css';
import { Dashboard } from "./pages/dashboard/Dashboard";
import { BrowserRouter, Navigate, Route, Routes, redirect } from "react-router-dom";
import { PrivateRoutes } from "./PrivateRoutes";
import axios from 'axios';
import { Landing } from "./pages/Landing/Landing";
import { createTheme } from "@mui/material";
import { ThemeProvider } from "@emotion/react";

axios.defaults.headers.common['Authorization'] = sessionStorage.getItem('token');

function App() {
  const darkTheme = createTheme({
    palette: {
      mode: 'dark',
      primary: {
        main: "#666bf0"
      },
      secondary: {
        main: '#6bb6f3'
      }
    },
  });

  return (
    <ThemeProvider theme={darkTheme}>
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Landing />} />
        <Route path="/access" element={<Access/>} />
        <Route exact path = "/dashboard" element={<PrivateRoutes />}>
          <Route path="/dashboard" element={<Dashboard />} />
        </Route>
        <Route path='*' element={<Navigate to='/' />} />
      </Routes>
    </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
