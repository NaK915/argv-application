import React from 'react'
import { Navigate, Outlet, Route, redirect } from 'react-router-dom'

export const PrivateRoutes = () => {
    const auth = sessionStorage.getItem('token');
    return auth ? <Outlet /> : <Navigate to="/access" />
}
