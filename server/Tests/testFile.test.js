const mongoose = require("mongoose");
const request = require("supertest");
const app = require("../index.js");
require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` })

// Variable to store details when required
let store = {};

/* Connecting to the database before each test. */
// beforeAll( async () => {
//     await mongoose.createConnection(process.env.MONGO_URL);
// });

/* Closing database connection after each test. */
afterAll( async () => {
    await mongoose.connection.collections['groups'].drop();
    await mongoose.connection.collections['profiles'].drop();
    await mongoose.connection.close();
});

// testing the test route
describe("GET /", () => {
    it("should return 200 OK", async () => {
      const res = await request(app).get('/')
      expect(res.statusCode).toBe(200);
    });
});

// Testing Auth routes
describe("POST /register", () => {
    it("should return error", async () => {
        const res = await request(app).post('/register').send({
            name: 'Jest Test',
            password: 'test'
        });
        expect(res.statusCode).toBe(400);
    });
    it("should return user details", async () => {
      const res = await request(app).post('/register').send({
        name: 'Jest Test',
        email: 'jt@email.com',
        password: 'test'
      });
      expect(res.statusCode).toBe(200);
      expect(res.body.user.name).toBe('Jest Test');
      expect(res.body.user.email).toBe('jt@email.com');
      store.user = res.body.user;
    });
    it("should create a new user", async () => {
        const res = await request(app).post('/register').send({
          name: 'Jest Test 2',
          email: 'jt2@email.com',
          password: 'test'
        });
        expect(res.statusCode).toBe(200);
        expect(res.body.user.name).toBe('Jest Test 2');
        expect(res.body.user.email).toBe('jt2@email.com');
        store.user_2 = res.body.user;
    });
    it("should return error", async () => {
        const res = await request(app).post('/register').send({
          name: 'Jest Test 2',
          email: 'jt2@email.com',
          password: 'test'
        });
        expect(res.statusCode).toBe(409);
        expect(res.body.message).toBe('User already exists');
    });
});

describe("POST /login", () => {
    it("should return error", async () => {
        const res = await request(app).post('/login').send({
            email: 'jt@email.com',
            password: 'tset'
        });
        expect(res.statusCode).toBe(404);
        expect(res.body.message).toBe('Incorrect Email-Password combination');
    });
    it("should return error", async () => {
        const res = await request(app).post('/login').send({
            email: 'tj@email.com',
            password: 'test'
        });
        expect(res.statusCode).toBe(404);
        expect(res.body.message).toBe('Incorrect Email-Password combination');
    });
    it("should login user and return JSON token", async () => {
      const res = await request(app).post('/login').send({
        email: 'jt@email.com',
        password: 'test'
      });
      expect(res.statusCode).toBe(200);
      expect(res.body._id).toBe(store.user._id);
      store.token = res.body.token;
    });
    it("should login second user and return JSON token", async () => {
        const res = await request(app).post('/login').send({
          email: 'jt2@email.com',
          password: 'test'
        });
        expect(res.statusCode).toBe(200);
        expect(res.body._id).toBe(store.user_2._id);
        store.token_2 = res.body.token;
    });
});


// Getting Profile information
describe("GET /profile", () => {
    it("should give auth not present error", async () => {
        const res = await request(app).get(`/profile?_id=${store.user._id}`)
        expect(res.statusCode).toBe(498);
        expect(res.body.message).toBe('Invalid Token');
    });
    it("should give profile", async () => {
      const res = await request(app).get(`/profile?_id=${store.user._id}`).set('Authorization', store.token);
      expect(res.statusCode).toBe(200);
      expect(res.body).toStrictEqual(store.user);
    });
    it("should give not found error", async () => {
        const res = await request(app).get(`/profile`).set('Authorization', store.token);
        expect(res.statusCode).toBe(404);
    });
});

describe("GET /profile/email", () => {
    it("should give profile", async () => {
      const res = await request(app).get(`/profile/email?email=${store.user_2.email}`).set('Authorization', store.token);
      expect(res.statusCode).toBe(200);
      expect(res.body).toStrictEqual(store.user_2);
    });
    it("should give not found error", async () => {
        const res = await request(app).get(`/profile/email`).set('Authorization', store.token);
        expect(res.statusCode).toBe(404);
    });
});

// Creating Group
describe("POST /group", () => {
    it("should create group and return group data", async () => {
      const res = await request(app).post('/group').set('Authorization', store.token).send({
        name: 'jest-test-group'
      });
      expect(res.statusCode).toBe(200);
      expect(res.body.name).toBe('jest-test-group');
      expect(res.body.author).toBe(store.user._id);
      store.group = res.body;
    });
    it("should return error", async () => {
        const res = await request(app).post('/group').send({
            name: 'jest-test-group'
        });
        expect(res.statusCode).toBe(498);
    });
});

// Get Details about group
describe("GET /group", () => {
    it("should give group information", async () => {
      const res = await request(app).get(`/group?_id=${store.group._id}`).set('Authorization', store.token);
      expect(res.statusCode).toBe(200);
      expect(res.body).toStrictEqual(store.group);
    });
    it("should give not found error", async () => {
        const res = await request(app).get(`/group`).set('Authorization', store.token);
        expect(res.statusCode).toBe(400);
        expect(res.body.message).toBe('Invalid ID passed');
    });
    it('should deny access', async () => {
        const res = await request(app).get(`/group?_id=${store.group._id}`).set('Authorization', store.token_2);
        expect(res.statusCode).toBe(403);
        expect(res.body.message).toBe('Access Denied');
    });
});

// Adding access to group
describe("POST /group/level", () => {
    it("should give error", async () => {
      const res = await request(app).post(`/group/level?_id=${store.group._id}`).set('Authorization', store.token).send({
        profile: store.extra_user
      });
      expect(res.statusCode).toBe(400);
      expect(res.body.message).toBe('Invalid ID passed');
    });
    it("should add new member to group", async () => {
        const res = await request(app).post(`/group/level?_id=${store.group._id}`).set('Authorization', store.token).send({
          profile: {
            name: store.user_2.name,
            _id: store.user_2._id,
            level: 1
          }
        });
        expect(res.statusCode).toBe(200);
        expect(res.body.name).toBe('jest-test-group');
        expect(res.body.author).toBe(store.user._id);
        expect(res.body.access.length).toBe(2);
        expect(res.body.access[1]._id).toBe(store.user_2._id);
        store.group = res.body;
    });
    it("should return error", async () => {
        const res = await request(app).post('/group').send({
            name: 'jest-test-group'
        });
        expect(res.statusCode).toBe(498);
    });
    it("should deny access", async () => {
        const res = await request(app).post(`/group/level?_id=${store.group._id}`).set('Authorization', store.token_2).send({
          profile: {
            name: store.user.name,
            _id: store.user._id,
            level: 2
          }
        });
        expect(res.statusCode).toBe(403);
        expect(res.body.message).toBe('Access Denied');
    });
});

// Changing Access to group
describe("PUT /group/level", () => {
    it("should give error", async () => {
      const res = await request(app).put(`/group/level?_id=${store.group._id}`).set('Authorization', store.token).send({
        profile: store.extra_user
      });
      expect(res.statusCode).toBe(400);
      expect(res.body.message).toBe('Invalid ID passed');
    });
    it("should deny access", async () => {
        const res = await request(app).put(`/group/level?_id=${store.group._id}`).set('Authorization', store.token_2).send({
            profile: {
                name: store.user_2.name,
                _id: store.user_2._id,
                level: 2
            }
        });
        expect(res.statusCode).toBe(403);
        expect(res.body.message).toBe('Access Denied');
    });
    it("should change level to 2", async () => {
        const res = await request(app).put(`/group/level?_id=${store.group._id}`).set('Authorization', store.token).send({
            profile: {
                name: store.user_2.name,
                _id: store.user_2._id,
                level: 2
            }
        });
        expect(res.statusCode).toBe(200);
        expect(res.body.name).toBe('jest-test-group');
        expect(res.body.access.length).toBe(2);
        expect(res.body.access[1].level).toBe(2);
        store.group = res.body;
    });
    it("should change level to 3", async () => {
        const res = await request(app).put(`/group/level?_id=${store.group._id}`).set('Authorization', store.token).send({
            profile: {
                name: store.user_2.name,
                _id: store.user_2._id,
                level: 3
            }
        });
        expect(res.statusCode).toBe(200);
        expect(res.body.name).toBe('jest-test-group');
        expect(res.body.access.length).toBe(2);
        expect(res.body.access[1].level).toBe(3);
        store.group = res.body;
    });
    it("should return error", async () => {
        const res = await request(app).put('/group/level').send({
            name: 'jest-test-group'
        });
        expect(res.statusCode).toBe(498);
    });
});

// Remove access from the group
describe("DELETE /group/level", () => {
    it("should return error", async () => {
        const res = await request(app).delete('/group/level');
        expect(res.statusCode).toBe(498);
    });
    it("should return error", async () => {
        const res = await request(app).delete(`/group/level?_id=${store.group._id}`).set('Authorization', store.token);
        expect(res.statusCode).toBe(400);
        expect(res.body.message).toBe('Invalid user ID passed');
    });
    it("should give error", async () => {
      const res = await request(app).delete(`/group/level?profile_id=${store.user_2._id}`).set('Authorization', store.token);
      expect(res.statusCode).toBe(400);
      expect(res.body.message).toBe('Invalid ID passed');
    });
    it("should deny access", async () => {
        const res = await request(app).delete(`/group/level?_id=${store.group._id}&profile_id=${store.user._id}`).set('Authorization', store.token_2);
        expect(res.statusCode).toBe(403);
        expect(res.body.message).toBe('Access Denied');
    });
    it("should delete member from group", async () => {
        const res = await request(app).delete(`/group/level?_id=${store.group._id}&profile_id=${store.user_2._id}`).set('Authorization', store.token);
        expect(res.statusCode).toBe(200);
        expect(res.body.name).toBe('jest-test-group');
        expect(res.body.access.length).toBe(1);
        expect(res.body.access[0].level).toBe(4);
        store.group = res.body;
    });
    // This is because we need to add the user for testing delete functionality
    it("should add new member to group", async () => {
        const res = await request(app).post(`/group/level?_id=${store.group._id}`).set('Authorization', store.token).send({
          profile: {
            name: store.user_2.name,
            _id: store.user_2._id,
            level: 1
          }
        });
        expect(res.statusCode).toBe(200);
        expect(res.body.name).toBe('jest-test-group');
        expect(res.body.author).toBe(store.user._id);
        expect(res.body.access.length).toBe(2);
        expect(res.body.access[1]._id).toBe(store.user_2._id);
        store.group = res.body;
    });
});

// Uploading files
describe("POST /upload & /upload/personal", () => {
    it("should return error", async () => {
        const res = await request(app).post('/upload');
        expect(res.statusCode).toBe(498);
    });
    it("should return error", async () => {
        const res = await request(app).post('/upload/personal');
        expect(res.statusCode).toBe(498);
    });
    it("should return error", async () => {
        const res = await request(app).post(`/upload?_id=${store.group._id}`).set('Authorization', store.token);
        expect(res.statusCode).toBe(400);
        expect(res.body.message).toBe('No file found');
    });
    it("should return error", async () => {
        const res = await request(app).post(`/upload/personal?_id=${store.user._id}`).set('Authorization', store.token);
        expect(res.statusCode).toBe(400);
        expect(res.body.message).toBe('No file found');
    });
    it("should deny access", async () => {
        const res = await request(app).post(`/upload?_id=${store.group._id}`).set('Authorization', store.token_2).attach('file', '/home/naman.k/Documents/argv/server/Tests/testFile.json');
        expect(res.statusCode).toBe(403);
        expect(res.body.message).toBe('Access Denied');
    });
    it("should upload file", async () => {
        const res = await request(app).post(`/upload?_id=${store.group._id}`).set('Authorization', store.token).attach('file', '/home/naman.k/Documents/argv/server/Tests/testFile.json');
        expect(res.statusCode).toBe(200);
        expect(res.body.files.length).toBe(1);
        expect(res.body.files[0].name).toBe('testFile.json');
    });
    it("should upload file", async () => {
        const res = await request(app).post(`/upload/personal?_id=${store.user._id}`).set('Authorization', store.token).attach('file', `/home/naman.k/Documents/argv/server/Tests/testFile.json`);
        expect(res.statusCode).toBe(200);
        expect(res.body.personal.length).toBe(1);
        expect(res.body.personal[0].name).toBe('testFile.json');
    });
});

// Updating files
describe("POST /upload & /upload/personal", () => {
    it("should update file", async () => {
        const res = await request(app).post(`/upload?_id=${store.group._id}`).set('Authorization', store.token).attach('file', '/home/naman.k/Documents/argv/server/Tests/testFile2/testFile.json');
        expect(res.statusCode).toBe(200);
        expect(res.body.files.length).toBe(1);
        expect(res.body.files[0].name).toBe('testFile.json');
        expect(res.body.files[0].created).not.toBe(res.body.files[0].modified);
    });
    it("should update file", async () => {
        const res = await request(app).post(`/upload/personal?_id=${store.user._id}`).set('Authorization', store.token).attach('file', `/home/naman.k/Documents/argv/server/Tests/testFile2/testFile.json`);
        expect(res.statusCode).toBe(200);
        expect(res.body.personal.length).toBe(1);
        expect(res.body.personal[0].name).toBe('testFile.json');
        expect(res.body.personal[0].created).not.toBe(res.body.personal[0].modified);
    });
});

// Get file URL
describe("GET /files & /files/personal", () => {
    it("should get url for files", async () => {
        await new Promise(async resolve => {
            const res = await request(app).get(`/files?_id=${store.group._id}&name=testFile.json`).set('Authorization', store.token);
            expect(res.statusCode).toBe(200);
            await setTimeout( async () => {
                const aws_response = await fetch(res.body.url)
                console.log(aws_response);
                expect(aws_response.status).toBe(403);
                resolve();
            }, 60000); 
        });
    });
    it("should get url for files", async () => {
        await new Promise(async resolve => {
            const res = await request(app).get(`/files/personal?_id=${store.user._id}&name=testFile.json`).set('Authorization', store.token);
            expect(res.statusCode).toBe(200);
            await setTimeout( async () => {
                const aws_response = await fetch(res.body.url)
                console.log(aws_response);
                expect(aws_response.status).toBe(403);
                resolve();
            }, 60000); 
        });
    });
});

// Remove files
describe("DELETE /files & /files/personal", () => {
    it("should give access denied error", async () => {
        const res = await request(app).delete(`/files?_id=${store.user._id}&file_name=testFile.json`).set('Authorization', store.token_2);
        expect(res.statusCode).toBe(403);
        expect(res.body.message).toBe('Access Denied');
    });
    it("should delete file", async () => {
        const res = await request(app).delete(`/files?_id=${store.group._id}&file_name=testFile.json`).set('Authorization', store.token);
        expect(res.statusCode).toBe(200);
        expect(res.body.files.length).toBe(0);
    });
    it("should delete file", async () => {
        const res = await request(app).delete(`/files/personal?_id=${store.user._id}&file_name=testFile.json`).set('Authorization', store.token);
        expect(res.statusCode).toBe(200);
        expect(res.body.personal.length).toBe(0);
    });
});


describe("DELETE /group", () => {
    it("should return access denied error", async () => {
      const res = await request(app).delete(`/group?_id=${store.group._id}`).set('Authorization', store.token_2);
      expect(res.statusCode).toBe(403);
      expect(res.body.message).toBe('Access Denied');
    });
    it("should delete group", async () => {
        const res = await request(app).delete(`/group?_id=${store.group._id}`).set('Authorization', store.token);
        expect(res.statusCode).toBe(200);
        expect(res.body.message).toBe('Successfully deleted');
    });
});