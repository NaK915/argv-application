const Profile = require('../Models/profile');
const Group = require('../Models/group');

const groupFileUpload = async (req, res) => {
    try{
        Group.findOne({_id: req.query._id}).then((result) => {
            const date = new Date();
            const exists = result.files.find((item) => item.name === req.files.file.name);
            if(exists){
                Group.findOneAndUpdate({_id: req.query._id}, {
                    $set: {'files.$[x].modified': date.toISOString(), 'files.$[x].modified_by': {email: req.user.email, _id: req.user._id}}
                }, {arrayFilters: [{'x.name': (req.files.file.name)}], new: true}).then((out) => {
                    console.log(out);
                    res.status(200).json(out.toJSON()).end();
                });
            } else {
                const updated_doc = {name: req.files.file.name, created: date.toISOString(), modified: date.toISOString(), modified_by: {email: req.user.email, _id: req.user._id}, author: {email: req.user.email, _id: req.user._id}, url: req.filePath};
                console.log(updated_doc);
                Group.findOneAndUpdate({_id: req.query._id}, {
                    $push: {files: updated_doc}
                }, {new: true}).then((out) => {
                    console.log(out);
                    res.status(200).json(out.toJSON()).end();
                });
            }
        });
    } catch(err) {
        res.status(500).json({message: err.message}).end();
    }
}

const personalFileUpload = async (req, res) => {
    try {
        Profile.findOne({_id: req.query._id}).then((result) => {
            const date = new Date();
            const exists = result.personal?.find((item) => item.name === req.files.file.name);
            if(exists){
                Profile.findOneAndUpdate({_id: req.query._id}, {
                    $set: {'personal.$[x].modified': date.toISOString(), 'personal.$[x].modified_by': {email: req.user.email, _id: req.user._id}}
                }, {arrayFilters: [{'x.name': (req.files.file.name)}], new: true}).then((out) => {
                    console.log(out);
                    res.status(200).json(out.toJSON()).end();
                });
            } else {
                const updated_doc = {name: req.files.file.name, created: date.toISOString(), modified: date.toISOString(), modified_by: {email: req.user.email, _id: req.user._id}, author: {email: req.user.email, _id: req.user._id}, url: req.filePath};
                console.log(updated_doc);
                Profile.findOneAndUpdate({_id: req.query._id}, {
                    $push: {personal: updated_doc}
                }, {new: true}).then((out) => {
                    console.log(out);
                    res.status(200).json(out.toJSON()).end();
                });
            }
        });
    } catch (err) {
        res.status(500).json({message: err.message}).end();
    }
}

module.exports = { groupFileUpload, personalFileUpload }