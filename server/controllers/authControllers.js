// Required Imports
const Profile = require('../Models/profile');
const { gen_hash, compare_hash, gen_jwt } = require('../Functions/authentication');

const register = async (req, res) => {
    Profile.findOne({email: req.body.email}).then(async (out) => {
        if(out!==null){
            res.status(409).json({message: 'User already exists'}).end();
            return ;
        } else {
            const date = new Date();
            const pass = await gen_hash(req.body.password);
            const doc = await {...req.body, password: pass, created: date.toISOString()}; 
            await Profile.create(doc).then((user) => {
                // console.log(user);
                res.status(200).json({user: user.toJSON()}).end();
            }).catch((err) => {
                console.log(err);
                res.status(400).json({message: err.message}).end();
            });
        }
    });
};

const login = async (req, res) => {
    Profile.findOne({email: req.body.email}).then(async (out) => {
        if(out===null){
            res.status(404).send({message: 'Incorrect Email-Password combination'}).end();
        } else {
            await compare_hash(req.body.password, out.password).then( async (valid) => {
                if(valid){
                    const token = await gen_jwt(out.toJSON());
                    res.status(200).json({token: token, _id: out._id}).end();
                } else {
                    res.status(404).send({message: 'Incorrect Email-Password combination'}).end();
                }
            });
        }
    });
}

module.exports = { register, login }