const { getFile } = require('../Functions/fileHandling');

const Profile = require('../Models/profile');
const Group = require('../Models/group');

const getURL = async (req, res) => {
    Group.findOne({_id: req.query._id}).then( async (out) => {
        console.log(out);
        let file = {};
        out.files.forEach((item) => {
            if(item.name === req.query.name){
                file = item;
            }
        });
        const url = await getFile(file.url);
        console.log(url);
        res.status(200).json({url: url}).end();
    })
}

const getPersonalFile = async (req, res) => {
    Profile.findOne({_id: req.query._id}).then(async(out) => {
        console.log(out);
        let file = {};
        out.personal.forEach((item) => {
            if(item.name === req.query.name){
                file = item;
            }
        });
        const url = await getFile(file.url);
        res.status(200).json({url: url}).end();
    })
}

module.exports = { getPersonalFile, getURL }