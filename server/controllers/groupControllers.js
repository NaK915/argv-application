const Group = require('../Models/group');
const Profile = require('../Models/profile');

const createGroup = async(req, res) => {
    console.log(req.user);
    const date = new Date();
    try {
        Group.create({
            name: req.body.name,
            created: date.toISOString(),
            files: [],
            author: req.user._id,
            access: [{
                name: req.user.name,
                _id: req.user._id,
                level: 4
            }]
        }).then((result) => {
            console.log(result);
            Profile.findOneAndUpdate({_id: req.user._id},
                {$push: {access: 
                    {_id: result._id, name: result.name, level: 4, created: date.toISOString()}
                }}, {new: true}).then((out) => {
                    console.log(out.toJSON());
                    res.status(200).json(result.toJSON()).end();
                });
        });
    } catch (err) {
        res.status(500).json({message: err.message}).end();
    }
}

const getGroup = async(req, res) => {
    Group.findOne({_id: req.query._id}).then((result) => {
        console.log(result);
        res.status(200).json(result).end();
    }).catch((err) => {
        res.status(500).json({message: err.message}).end();
    });
}

const deleteGroup = async(req, res) => {
    if(req.level !== 4){
        res.status(403).json({message: "Access Denied"}).end();
    } else {
        Group.findOneAndDelete({_id: req.query._id}).then( async (out) => {
            await out.access.forEach( async (pr) => {
                await Profile.updateOne({_id: pr._id}, {
                    $pull: {access: {_id: out._id}}
                });
            })
            res.status(200).json({message: 'Successfully deleted'}).end();
        }).catch((err) => {
            res.status(500).json({message: err.message}).end();
        });
    }
}

module.exports = { createGroup, getGroup, deleteGroup }