const mongoose = require('mongoose');

const Profile = require('../Models/profile');
const Group = require('../Models/group');

const updateAccess = async (req, res) => {
    if(req.query._id === undefined || req.body.profile === undefined){
        res.status(400).json({message: 'Invalid ID passed'}).end();
    } else {
        if(req.level === 4){
            try {
                Group.findOneAndUpdate({_id: req.query._id}, {
                    $set: {
                        'access.$[x]': req.body.profile
                    }}, {arrayFilters: [{'x._id': req.body.profile._id}], new:true}
                ).then((out) => {
                    Profile.findOneAndUpdate({_id: req.body.profile._id}, {
                        $set: {
                            'access.$[x].level': req.body.profile.level
                        }}, {arrayFilters: [{'x._id': new mongoose.Types.ObjectId(req.query._id)}]}
                    ).then((r) => {
                        console.log(r);
                        res.status(200).json(out.toJSON()).end();
                    });
                });
            } catch (err) {
                res.status(500).json({message: err.message}).end();
            }
        } else {
            res.status(403).json({message: "Access Denied"}).end();
        }
    }
}

const addAccess = async (req, res) => {
    if(req.query._id === undefined || req.body.profile === undefined){
        res.status(400).json({message: 'Invalid ID passed'}).end();
    } else {
        if(req.level === 4){
            try {
                Group.findOneAndUpdate({_id: req.query._id}, {
                    $push: {
                        access: req.body.profile
                    }}, {new:true}
                ).then((out) => {
                    const data = {
                        _id: out._id,
                        name: out.name,
                        level: req.body.profile.level,
                        created: out.created
                    }
                    console.log(data);
                    Profile.findOneAndUpdate({_id: req.body.profile._id}, {
                        $push: {
                            access: data
                        }}
                    ).then((r) => {
                        console.log(r);
                        res.status(200).json(out.toJSON()).end();
                    });
                });
            } catch (err) {
                res.status(500).json({message: err.message}).end();
            }
        } else {
            res.status(403).json({message: "Access Denied"}).end();
        }
    }
}

const removeAccess = async (req, res) => {
    console.log(req.query.profile_id);
    if(req.query.profile_id){
        if(req.level === 4){
            try {
                Group.findOneAndUpdate({_id: req.query._id}, {
                    $pull: {
                        access: {_id : req.query.profile_id}
                    }}, {new:true}
                ).then((out) => {
                    Profile.findOneAndUpdate({_id: req.query.profile_id}
                        , {
                        $pull: {
                            access: {_id : new mongoose.Types.ObjectId(req.query._id)}
                        }}, {new: true}
                    ).then((r) => {
                        // console.log(r);
                        res.status(200).json(out.toJSON()).end();
                    });
                });
            } catch (err) {
                res.status(500).json({message: err.message}).end();
            }
        } else {
            res.status(403).json({message: "Access Denied"}).end();
        }
    } else {
        res.status(400).json({message: 'Invalid user ID passed'}).end();
    }
}

module.exports = { addAccess, removeAccess, updateAccess }