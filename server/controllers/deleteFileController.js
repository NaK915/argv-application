const Profile = require('../Models/profile');
const Group = require('../Models/group');

const deleteGroupFile = async (req, res) => {
    console.log(req.level);
    console.log(req.query);
    Group.findOneAndUpdate({_id: req.query._id}, {
        $pull: {files: {name: req.query.file_name}}
    }, {new: true}).then((response) => {
        // console.log(response);
        res.status(200).json(response.toJSON()).end();
    }).catch((err) => {
        res.status(500).json({message: err.message}).end();
    });
}

const deletePersonalFile = async (req, res) => {
    console.log(req.query);
    Profile.findOneAndUpdate({_id: req.query._id}, {
        $pull: {personal: {name: req.query.file_name}}
    }, {new: true}).then((response) => {
        console.log(response);
        res.status(200).json(response.toJSON()).end();
    }).catch((err) => {
        res.status(500).json({message: err.message}).end();
    });
}

module.exports = { deleteGroupFile, deletePersonalFile }