const Profile = require('../Models/profile');

const getProfileViaId = async (req, res) => {
    Profile.findOne({_id: req.query._id}).then(async (out) => {
        res.status(200).json(out.toJSON()).end();
    }).catch((err) => {
        console.log(err);
        res.status(404).json({message: 'No User found'}).end();
    });
}

const getProfileViaEmail = async (req, res) => {
    console.log(req.query.email);
    Profile.findOne({email: req.query.email}).then(async (out) => {
        res.status(200).json(out.toJSON()).end();
    }).catch((err) => {
        console.log(err);
        res.status(404).json({message: 'No User found'}).end();
    });
}

module.exports = { getProfileViaId, getProfileViaEmail }