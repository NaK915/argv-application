const mongoose = require('mongoose');

const groupSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  created: {
    type: Date,
    required: true
  },
  author: {
    type: String,
    required: true
  },
  files: {
    type: [Object],
    required: true
  },
  access: {
    type: [Object],
    required: true
  }
});

const Group = mongoose.model('Group', groupSchema);
module.exports =  Group;