const mongoose = require('mongoose');

const profileSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  created: {
    type: Date,
    required: true
  },
  image: {
    type: String,
  },
  password: {
    type: String,
    required: true
  },
  personal: {
    type: [Object],
    required: true
  },
  access: {
    type: [Object],
    required: true
  }
});

const Profile = mongoose.model('Profile', profileSchema);
module.exports =  Profile;