// Importing required modules
const express = require('express');
const path = require('path');
const cors = require('cors');
const mongoose = require('mongoose');
const fileUpload = require('express-fileupload');

require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` });

// Importing Middlewares
const { auth, levelAccess } = require('./Functions/authentication');
const {uploadFilePersonal, uploadFile, deleteFilePersonal, deleteFile} = require('./Functions/fileHandling');

// Importing Controllers
const { register, login } = require('./controllers/authControllers');
const { getProfileViaId, getProfileViaEmail } = require('./controllers/profileControllers');
const { createGroup, deleteGroup, getGroup } = require('./controllers/groupControllers');
const { updateAccess, addAccess, removeAccess } = require('./controllers/groupLevelController');
const { groupFileUpload, personalFileUpload } = require('./controllers/fileUploadController');
const { getURL, getPersonalFile } = require('./controllers/fileURLController');
const { deleteGroupFile, deletePersonalFile } = require('./controllers/deleteFileController');

// Setting up Express
const app = express();
const router = express.Router();
app.use(cors());
app.use(fileUpload());
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// Connecting to MongoDB
mongoose.connect(process.env.MONGO_URL).then(() => {
    console.log('Connected!');
});

// Test Route
app.get('/', async (req, res) => {
    res.send('Hello World!').end();
});

// --- Authentication Routes ---

// Register a new user
app.post('/register', register);

// Login an existing user
app.post('/login', login);


// --- Profile Route ---

// Get profile via unique ID
app.get('/profile', auth, getProfileViaId);

// Get profile via Email ID
app.get('/profile/email', auth, getProfileViaEmail);

// --- Group Functions ---

// Create new group
app.post('/group', auth, createGroup);

// Get details about group
app.get('/group', auth, levelAccess, getGroup);

// Delete a group
app.delete('/group', auth, levelAccess, deleteGroup);


// --- Group Level Functions ---

// Update current group levels
app.put('/group/level', auth, levelAccess, updateAccess);

// Add new member to group
app.post('/group/level', auth, levelAccess, addAccess);

// Remove Access from group
app.delete('/group/level', auth, levelAccess, removeAccess);

// --- File Functions ---

// File Upload to group
app.post('/upload', auth, levelAccess, uploadFile, groupFileUpload);

// File upload for personal storage
app.post('/upload/personal', auth, uploadFilePersonal, personalFileUpload);

// Get group access file
app.get('/files', auth, levelAccess, getURL);

// Get files from personal vault
app.get('/files/personal', auth, getPersonalFile);

// Delete files from group
app.delete('/files', auth, levelAccess, deleteFile, deleteGroupFile);

// Delete files from personal vault
app.delete('/files/personal', auth, deleteFilePersonal, deletePersonalFile);


// App listening
app.listen(process.env.PORT, () => {
    console.log(`Example app listening on port ${process.env.PORT}`);
});

module.exports = app