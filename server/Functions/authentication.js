// Importing required modules
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Profile = require('../Models/profile');
require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` })

const gen_hash = async (password) => {
    const hash = await bcrypt.hash(password, Number(process.env.SALT_ROUNDS));
    return hash;
}

const compare_hash = async (user_pass, db_pass) => {
    const result = await bcrypt.compare(user_pass, db_pass);
    return result;
}

const gen_jwt = async (user) => {
    const accessToken = await jwt.sign(user, process.env.SECRET, {expiresIn: process.env.TIME});
    return accessToken;
}

const validate_jwt = async (token) => {
    const result = await jwt.verify(token, process.env.SECRET);
    return result;
}

const auth = async (req, res, next) => {
    const authHeader = req.header('Authorization');

    try{
        const valid = await validate_jwt(authHeader);
        console.log(valid);
        Profile.findOne({_id: valid._id, password: valid.password}).then((response) => {
            if(response===null){
                res.status(401).json({message: 'Invalid User'}).end();
            } else {
                req.user = response;
                next();
            }
        });
    } catch (err) {
        res.status(498).json({message: "Invalid Token"}).end();
    }
}

const levelAccess = async (req, res, next) => {
    if(req.query._id !== undefined){
        let level = 0;
        await req.user.access.forEach((item) => {
            console.log(item._id);
            console.log(req.query._id);
            if(item._id.equals(req.query._id)){
                level = item.level;
            }
        });
        if(level !== 0){
            req.level = level;
            next();
        } else {
            console.log('WE ARE HERE ARE ARE ARAE ARE REREJRLKJERJLEJRLKEJR');
            res.status(403).json({message: "Access Denied"}).end();
        }
    } else {
        res.status(400).json({message: 'Invalid ID passed'}).end();
    }
}

module.exports = {gen_hash, gen_jwt, compare_hash, auth, levelAccess}