// Importing Required Modules
const { S3Client, PutObjectCommand, GetObjectCommand, DeleteObjectCommand } = require("@aws-sdk/client-s3");
const { getSignedUrl } = require("@aws-sdk/s3-request-presigner");
const https = require('https');
require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` })

// Function to create presigned URL with client
const createPresignedURL = ({ accessKeyId, secretAccessKey, region, command }) => {
    const client = new S3Client({ credentials: { accessKeyId, secretAccessKey }, region });
    return getSignedUrl(client, command, { expiresIn: 60 });
    // Here, expiresIn is Seconds before expiry count
};

// Function to put the data into the request
const put = (url, data) => {
    return new Promise((resolve, reject) => {
    const req = https.request(
        url,
        { method: "PUT", headers: { "Content-Length": new Blob([data]).size } },
        (res) => {
            let responseBody = "";
            res.on("data", (chunk) => {
                responseBody += chunk;
            });
            res.on("end", () => {
                resolve(responseBody);
            });
        },
    );
    req.on("error", (err) => {
        reject(err);
    });
    req.write(data);
        req.end();
    });
}

const uploadFile = async (req, res, next) => {
    if(req.files && req.files.file){
        if(req.level>1){
            const fileContent  = Buffer.from(req.files.file.data, 'binary');
            const accessKeyId = process.env.AWS_ACCESS_KEY_ID;
            const secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY;
            const region = process.env.S3_REGION;
            const Bucket = process.env.S3_BUCKET;
            const filePath = `${req.query._id}/${req.files.file.name}`;
            const command = new PutObjectCommand({ Bucket: Bucket, Key: filePath });
            try {
                const url = await createPresignedURL({
                    accessKeyId: accessKeyId,
                    secretAccessKey: secretAccessKey,
                    region: region,
                    command: command
                });
                await put(url, fileContent);
                req.filePath = filePath;
                next();
            } catch(err) {
                res.status(500).json({message: err.message}).end();
            }
        } else {
            res.status(403).json({message: 'Access Denied'}).end();
        }
    } else {
        res.status(400).json({message: 'No file found'}).end();
    }
    
}

const uploadFilePersonal = async (req, res, next) => {
    if(req.files && req.files.file){
        const fileContent  = Buffer.from(req.files.file.data, 'binary');
        const accessKeyId = process.env.AWS_ACCESS_KEY_ID;
        const secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY;
        const region = process.env.S3_REGION;
        const Bucket = process.env.S3_BUCKET;
        const filePath = `personal/${req.query._id}/${req.files.file.name}`;
        const command = new PutObjectCommand({ Bucket: Bucket, Key: filePath });
        try {
            const url = await createPresignedURL({
                accessKeyId: accessKeyId,
                secretAccessKey: secretAccessKey,
                region: region,
                command: command
            });
            await put(url, fileContent);
            req.filePath = filePath;
            next();
        } catch(err) {
            res.status(500).json({message: err.message}).end();
        }
    } else {
        res.status(400).json({message: 'No file found'}).end();
    }
}

const getFile = async (filePath) => {
    const accessKeyId = process.env.AWS_ACCESS_KEY_ID;
    const secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY;
    const region = process.env.S3_REGION;
    const Bucket = process.env.S3_BUCKET;
    const command = new GetObjectCommand({ Bucket: Bucket, Key: filePath });

    try {
        const url = await createPresignedURL({
            accessKeyId: accessKeyId,
            secretAccessKey: secretAccessKey,
            region: region,
            command: command
        });
        return url;
    } catch(err) {
        throw err;
    }
}

const deleteFile = async (req, res, next) => {
    if(req.level >= 3){
        const accessKeyId = process.env.AWS_ACCESS_KEY_ID;
        const secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY;
        const region = process.env.S3_REGION;
        const Bucket = process.env.S3_BUCKET;
        const filePath = `${req.query._id}/${req.query.file_name}`;
        
        const client = new S3Client({
            credentials: {
                accessKeyId,
                secretAccessKey
            },
            region
        });

        const params = {
            Bucket: Bucket,
            Key: filePath
        }

        const command = new DeleteObjectCommand(params);
        await client.send(command).then((response) => {
            console.log(response);
            next();
        }).catch((err) => {
            res.status(500).json({message: err.message}).end();
        });
    } else {
        // console.log('We are here');
        res.status(403).json({message: 'Access Denied'}).end();
    }
}

const deleteFilePersonal = async (req, res, next) => {
    const accessKeyId = process.env.AWS_ACCESS_KEY_ID;
    const secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY;
    const region = process.env.S3_REGION;
    const Bucket = process.env.S3_BUCKET;
    const filePath = `personal/${req.query._id}/${req.query.file_name}`;
    
    const client = new S3Client({
        credentials: {
            accessKeyId,
            secretAccessKey
        },
        region
    });

    const params = {
        Bucket: Bucket,
        Key: filePath
    }

    const command = new DeleteObjectCommand(params);
    await client.send(command).then((response) => {
        console.log(response);
        next();
    }).catch((err) => {
        res.status(500).json({message: err.message}).end();
    });
}

module.exports = {getFile, uploadFile, uploadFilePersonal, deleteFile, deleteFilePersonal}